clear;clc;
format long;

function[res] = fx(funcion, vs)
    res=[];
    for x=vs
        y=eval(funcion);
        res=[res y];  
    end
end

function[ret] = formulaA(tb,h)
    ret=[];
        denominador = 2*h*h*h;
    for i=5:(length(tb))-4
        numerador = -5*tb(i)+18*tb(i+1)-24*tb(i+2)+14*tb(i+3)-3*tb(i+4);
        ret=[ret numerador/denominador];
    end
end
function[ret] = formulaB(tb,h)
    ret=[];
        denominador = 8*h*h*h;
    for i=5:(length(tb))-4
        numerador = tb(i-3)-8*tb(i-2)+13*tb(i-1)-13*tb(i+1)+8*tb(i+2)-tb(i+3);
        ret=[ret numerador/denominador];
    end
end
function[ret] = formulaC(tb,h)
    ret=[];
        denominador = 2*h*h*h;
    for i=5:(length(tb))-4
        numerador = 3*tb(i-4)-14*tb(i-3)+24*tb(i-2)-18*tb(i-1)+5*tb(i);
        ret=[ret numerador/denominador];
    end
end


ecuacion = "3*x^4+5*x^3-8*x+12";
inicial = 0;
final = 10;
iteraciones = 50;
valH = (final-inicial)/iteraciones;
rango = (inicial-4*valH:valH:final+4*valH);


tb = fx(ecuacion,rango)(:);

derivadas = formulaA(tb, valH);
derivadasa = formulaB(tb, valH);
derivadasb = formulaC(tb, valH);

rango = rango(5:length(rango)-4);
data = [rango(:) derivadas(:) derivadasa(:) derivadasb(:)];


VarNames = {'x', 'F1', 'F2', 'F3'};
fprintf(1, '\t%s\t\t%s\t\t%s\t\t%s\n', VarNames{:});
fprintf(1, '\t%.2f\t\t%.2f\t\t%.2f\t\t%.2f\n', data');