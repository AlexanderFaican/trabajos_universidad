clear; clc;
format long;
a = 0;
b = 3;
N = 20;
h = (b-a)/N;
x = a:h:b;
y(1) = 4;
for i=1:N
    k1 = h*fun(x(i),y(i));
    k2 = h*fun(x(i)+(h/4),y(i)+(k1/4));
    k3 = h*fun(x(i)+((3/8)*h),y(i)+((3/32)*k1)+((9/32)*k2));
    k4 = h*fun(x(i)+((12/13)*h),y(i)+((1932/2197)*k1)-((7200/2197)*k2)+((7296/2197)*k3));
    k5 = h*fun(x(i)+h,y(i)+((439/216)*k1)-(8*k2)+((3608/513)*k3)-((845/4101)*k4));
    k6 = h*fun(x(i)+(h/2),y(i)-((8/27)*k1)+(2*k2)-((3544/2565)*k3)+((1859/4101)*k4)-((11/40)*k5));
    y(i+1) = y(i)+(((16/135)*k1)+((6656/12825)*k3)+((28561/56430)*k4)-((9/50)*k5)+((2/55)*k6));
end
Metodo = y(:);

for Iter=1:N+1
    y(Iter) = Solucion_Solex(x(Iter));
end
Metodo_K=metodo_R(h,x,N);
Metodo_Runge_Kutta=Metodo_K(:);
Resp_Solex = y(:);
x=x(:);
disp(table(x,Metodo_Runge_Kutta,Resp_Solex));
function f = fun(t,y)
    f = (y+1)*(t+1)*cos((t^2)+(2*t));
end
function f = Solucion_Solex(t)
    f = 5*exp((1/2)*sin((t^2)+(2*t)))-1;
end

function [y]= metodo_R(h,x,N)
y(1) = 4;
for i=1:N
    k1 = fun(x(i),y(i));
    k2 = fun(x(i)+(h/2),y(i)+((h/2)*k1));
    k3 = fun(x(i)+(h/2),y(i)+((h/2)*k2));
    k4 = fun(x(i)+h,y(i)+(h*k3));
    y(i+1) = y(i)+((h*(k1+(2*k2)+(2*k3)+k4))/6);
end
end